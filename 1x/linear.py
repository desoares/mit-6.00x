def linear(aList, searched):
    '''
    A regular search
    aList an out of orde list
    '''
    size = len(aList)
    
    for n in range(size):
        if aList[n] == searched:
            return True
    
    return False