def nfruits(dic, patt):
    '''
    dic a non empty dictionary of fuit types
    patt a fruit consuption string pattern
    
    For each fruit pattern but the last, add one of every
    other fruit and subtract one of the consumed one
    '''
    #assert nom empty values for the arguments
    assert len(dic) > 0 and len (patt) > 0, 'Bad argument error'
    
    #get the list of different keys in the dictionary
    keys = dic.keys()
    
    #recursive escape case
    if len(patt) == 1:
        dic[patt[0]] -= 1
        return greater(dic)

    #add one for every key in dictionary
    for key in keys:  
            dic[key] += 1
    
    #subtract two of the consumed fruit easyer than make if statement
    dic[patt[0]] -= 2  
    return nfruits(dic, patt[1:])

def greater(dic):
    '''
    dic a dictionary key: int
    return the biggest int in a dictionary of key: int
    '''
    #list he keys in the dictionary
    keys = dic.keys()

    #recursion escape case
    if len(dic) == 1:
        return dic[keys[0]]

    #find the smaller key between 0 and 1
    result = int(dic[keys[0]] > dic[keys[1]])

    #delete the smaller
    del dic[keys[result]]

    #calls it self with smaller dic
    return greater(dic)