def iterPower(base, exp):
    '''
    base: int or float.
    exp: int >= 0
 
    returns: int or float, base^exp
    '''
    buff = base
    if exp == 0:
        return 1
        
    while (exp > 1):
        exp -= 1;
        buff = buff * base

    return buff
'''
def interPower(base, exp):
    if exp == 0:
        return 1
        
    return base * interPower(base, exp - 1)
'''    

test = iterPower(2, 1)
print test