# -*- coding: utf-8 -*-
def selSort(L):
    '''
    sorts a list using selection sort
    aList a non sorted list
    '''
    length = len(L)

    for i in range(length):
        minIndx = i
        minVal = L[i]
        j = i + 1
        
        while j < length:
            if minVal > L[j]:
                minIndx = j
                minVal = L[j]
            
            j += 1
        
        temp = L[i]
        L[i] = L[minIndx]
        L[minIndx] = temp
        
nameList = ["Fabio","Cae","Aclenis","Tuane","Willy","Aclenis"]
selSort(nameList)
print nameList

def newSort(L):
    leng = len(L)
    
    for i in range(leng- 1):
        j = i + 1
        
        while j < leng:
            if L[i] > L[j]:
                temp = L[i]
                L[i] = L[j]
                L[j] = temp
            j += 1
            
nameList = ["Fabio","Cae","Aclenis","Tuane","Willy","Angela", "Aclenis"]
newSort(nameList)
print nameList

def merge(left, right, compare):
    result = []
    i, j = 0, 0
    leftLen = len(left)
    rightLen = len(right)

    while i < leftLen and j < rightLen:
        if compare(left[i], right[j]):
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            j += 1
    
    while (i < leftLen):
        result.append(left[i])
        i += 1
    
    while (j < rightLen):
        result.append(right[j])
        j += 1
    
    return result
    
import operator

def mergeSort (L, compare = operator.lt):
    length = len(L)
    if length < 2:
        return L[:]
    
    else:
        middle = int(length/2)
        left = mergeSort(L[:middle], compare)
        right = mergeSort(L[middle:], compare)
        return merge(left, right, compare)

names = ["Xavier", "Scott", "Grey", "Macoy", "Logan", "Qwerty", "Adams"]
print mergeSort(names)
        