import math

def sqr(x):
    return x**2

class Coordinate(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def __str__(self):
        return "<" + str(self.x) + ", " + str(self.y) + ">"
    
    def distance(self, other):
        return math.sqrt(sqr(self.x - other.x) + sqr(self.y - other.y))
    
c = Coordinate(5, 1)
d = Coordinate(2, 5)
print isinstance(c, Coordinate)
print c.distance(d)