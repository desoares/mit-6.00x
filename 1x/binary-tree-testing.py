import BinaryTree, BinaryTreeSearch



n1 = BinaryTree.BinaryTree('a')
n2 = BinaryTree.BinaryTree('b')
n3 = BinaryTree.BinaryTree('c')
n4 = BinaryTree.BinaryTree('d')
n5 = BinaryTree.BinaryTree('e')
n6 = BinaryTree.BinaryTree('f')
n7 = BinaryTree.BinaryTree('g')
n8 = BinaryTree.BinaryTree('h')
n9 = BinaryTree.BinaryTree('i')
n10 = BinaryTree.BinaryTree('j')

n1.setLeftBranch(n2)
n1.setRightBranch(n3)
n2.setLeftBranch(n4)
n3.setLeftBranch(n5)
n2.setRightBranch(n6)
n3.setRightBranch(n7)
n8.setChildren(n9, n10)

print n1
print n8

print BinaryTreeSearch.depthFirstSearch(n1, BinaryTreeSearch.callBack, 'a')
print BinaryTreeSearch.breadthFirstSearch(n1, BinaryTreeSearch.callBack, 'h')
print BinaryTreeSearch.breadthFirstSearch(n1, BinaryTreeSearch.callBack, 'x')
print BinaryTreeSearch.breadthFirstSearch(n1, BinaryTreeSearch.callBack, 'i')
print BinaryTreeSearch.depthFirstSearch(n1, BinaryTreeSearch.callBack, 'b')
print BinaryTreeSearch.depthFirstSearch(n1, BinaryTreeSearch.callBack, 'c')
print BinaryTreeSearch.depthFirstSearch(n1, BinaryTreeSearch.callBack, 'd')
print BinaryTreeSearch.depthFirstSearch(n1, BinaryTreeSearch.callBack, 'e')
print BinaryTreeSearch.depthFirstSearch(n1, BinaryTreeSearch.callBack, 'f')
print BinaryTreeSearch.depthFirstSearch(n1, BinaryTreeSearch.callBack, 'g')
print BinaryTreeSearch.depthFirstSearch(n8, BinaryTreeSearch.callBack, 'h')
print BinaryTreeSearch.depthFirstSearch(n8, BinaryTreeSearch.callBack, 'i')
print BinaryTreeSearch.depthFirstSearch(n8, BinaryTreeSearch.callBack, 'j')

print "|||||||||||||||||||||||||||||||||||||||||||||||||||||||||"

print BinaryTreeSearch.breadthFirstSearch(n8, BinaryTreeSearch.callBack, 'h')
print BinaryTreeSearch.breadthFirstSearch(n8, BinaryTreeSearch.callBack, 'i')
print BinaryTreeSearch.breadthFirstSearch(n8, BinaryTreeSearch.callBack, 'j')
print BinaryTreeSearch.breadthFirstSearch(n8, BinaryTreeSearch.callBack, 'a')
print BinaryTreeSearch.breadthFirstSearch(n8, BinaryTreeSearch.callBack, 'b')
print BinaryTreeSearch.breadthFirstSearch(n8, BinaryTreeSearch.callBack, 'c')

print BinaryTreeSearch.depthFirstSearchPath(n1, BinaryTreeSearch.callBack, 'g')

it1 = BinaryTree.BinaryTree(1)
it2 = BinaryTree.BinaryTree(2)
it3 = BinaryTree.BinaryTree(3)
it4 = BinaryTree.BinaryTree(4)
it5 = BinaryTree.BinaryTree(5)
it6 = BinaryTree.BinaryTree(6)
it7 = BinaryTree.BinaryTree(7)
it8 = BinaryTree.BinaryTree(8)
it9 = BinaryTree.BinaryTree(9)
it10 = BinaryTree.BinaryTree(10)

it6.setChildren(it3, it8)
it3.setChildren(it2, it5)
it2.setLeftBranch(it1)
it5.setLeftBranch(it4)
it8.setChildren(it7, it9)
it9.setRightBranch(it10)
print it6
print BinaryTreeSearch.depthFirstOrdered(it6, BinaryTreeSearch.callBack, BinaryTreeSearch.ltCallBack, 1)
print BinaryTreeSearch.depthFirstOrdered(it6, BinaryTreeSearch.callBack, BinaryTreeSearch.ltCallBack, 2)
print BinaryTreeSearch.depthFirstOrdered(it6, BinaryTreeSearch.callBack, BinaryTreeSearch.ltCallBack, 3)
print BinaryTreeSearch.depthFirstOrdered(it6, BinaryTreeSearch.callBack, BinaryTreeSearch.ltCallBack, 4)
print BinaryTreeSearch.depthFirstOrdered(it6, BinaryTreeSearch.callBack, BinaryTreeSearch.ltCallBack, 5)
print BinaryTreeSearch.depthFirstOrdered(it6, BinaryTreeSearch.callBack, BinaryTreeSearch.ltCallBack, 6)
print BinaryTreeSearch.depthFirstOrdered(it6, BinaryTreeSearch.callBack, BinaryTreeSearch.ltCallBack, 7)
print BinaryTreeSearch.depthFirstOrdered(it6, BinaryTreeSearch.callBack, BinaryTreeSearch.ltCallBack, 8)
print BinaryTreeSearch.depthFirstOrdered(it6, BinaryTreeSearch.callBack, BinaryTreeSearch.ltCallBack, 9)
print BinaryTreeSearch.depthFirstOrdered(it6, BinaryTreeSearch.callBack, BinaryTreeSearch.ltCallBack, 10)
print BinaryTreeSearch.depthFirstOrdered(it6, BinaryTreeSearch.callBack, BinaryTreeSearch.ltCallBack, 2.5)
print BinaryTreeSearch.depthFirstOrdered(it6, BinaryTreeSearch.callBack, BinaryTreeSearch.ltCallBack, 11)
print BinaryTreeSearch.depthFirstOrdered(it6, BinaryTreeSearch.callBack, BinaryTreeSearch.ltCallBack, -3)

import BuildDecisionTree
print BuildDecisionTree.BuildDecisionTree()
a = [6, 3]
b = [7, 2]
c = [8, 4]
d = [9, 5]

test = BuildDecisionTree.BuildDecisionTree
print test([], [a, b, c, d])