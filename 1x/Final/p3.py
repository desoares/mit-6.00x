d = {1:10, 2:20, 3:30, 4:10}
d = {8: 6, 2: 6, 4: 6, 6: 6}

def dict_invert(d):
    '''
    d: dict
    Returns an inverted dictionary according to the instructions above
    '''
    my_list = []
    keys = d.values()
    ret_dic = {}

    for key in keys:
       if key not in my_list:
           ret_dic[key] = []
    
    for item in d:
       ret_dic[d[item]].append(item)
       ret_dic[d[item]].sort()


    return ret_dic

print dict_invert(d)