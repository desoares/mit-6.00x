def getSublists(L, n):
    ret_L = []
    size = len(L) - (n - 1)
    for i in range(size):
        ret_L.append(L[i:i+n])

    return ret_L

def longestRun(L):
    
    if len(L) == 1:
        return 1
    
    length = len(L)
    hi = 1
    test = 2

    while test <= length:
        tester = getSublists(L, test)
        
        for li in tester:
            count = 1

            while count < test - 1:
                if li[count] > li[count + 1]:
                    break

                count += 1            
            
            if count > hi:
                hi = count

        test += 1
    
    return hi

L = [10, 4, 6, 8, 3, 4, 5, 7, 7, 8, 2]
#L = [1,2,3,4, 3,3,3,3,3, 5,7,7,4,1,2,3,4,5,6,7]
L = [1,1,1,1,1,-1]
print longestRun(L)