def getSublists(L, n):
    ret_L = []
    size = len(L) - (n - 1)
    for i in range(size):
        ret_L.append(L[i:i+n])

    return ret_L
    
L = [10, 4, 6, 8, 3, 4, 5, 7, 7, 2]
n = 4
print getSublists(L, n)

L = [1, 1, 1, 1, 4]
n = 2
print getSublists(L, n)