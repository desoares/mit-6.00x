def getAvailableLetters(lettersGuessed):
    '''
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters that represents what letters have not
      yet been guessed.
    '''
    alphabet = 'abcdefghijklmnopqrstuvwxyz'
    availableAlph = ''

    for letter in alphabet:

        if letter not in lettersGuessed:
            availableAlph += letter
    
    return availableAlph
    
    
guess = ['a', 'z']

test = getAvailableLetters(guess)

print test