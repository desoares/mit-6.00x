import string

upper = string.ascii_uppercase
lower = string.ascii_lowercase

str = """If sep is not specified or is None, a different splitting algorithm is applied: runs of consecutive 
whitespace are regarded as a single separator, and the result will contain no empty strings at the start or 
end if the string has leading or trailing whitespace. Consequently, splitting an empty string or a string 
consisting of just whitespace with a None separator returns O'neil []"""
str = "hello"

strList = str.split(" ")
clean_list = []
result = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
for word in strList:
    nw = word
    for char in word:
        if char not in upper and char not in lower:
            nw = word.strip("1234567890=\\|,.;/<>:?\"!@#$%*()_+\n[]{}")
    clean_list.append(nw)

print clean_list
print len(result)