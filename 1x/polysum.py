import math

def polysum(n, s):
    per = n * s
    perSqr = per**2
    area = (0.25 * n * (s**2))/math.tan(math.pi/n)
    
    return round(perSqr + area, 4)
    
polysum(1,2)