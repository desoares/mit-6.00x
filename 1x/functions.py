def foo(x):
   def bar(x):
      return x + 1
   return bar(x * 2)

print foo(3)