class Coordinate(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def getX(self):
        return self.x
    
    def getY(self):
        return self.y
    
    def __str__(self):
        return '<' + str(self.getX()) + ', ' + str(self.getY()) + '>'
        
    def __eq__(self, other):
        return self.x == other.x and self.y == other.y
        
    def __repr__(self):
        return "Coordinate("+ str(self.getX()) +", "+ str(self.getY()) +")"

test = Coordinate(1,2)
test2 = Coordinate(2,8)

print test.__eq__(test2)
print test.__repr__()