def isWordGuessed(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: boolean, True if all the letters of secretWord are in lettersGuessed;
      False otherwise
    '''
    goal = len(secretWord)
    for letter in lettersGuessed:
        if letter in secretWord:
            subt = secretWord.count(letter)
            goal -= subt
            
    if goal <= 0:
        return True
    
    return False
    
word = 'cooperacao'
guess = ['a', 'e', 'i', 'o', 'c', 'p', 'r', 'f']

test = isWordGuessed(word, guess)

print test