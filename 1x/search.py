def search(aList, element):
    '''
    linear search on a non sorted list
    aList a list
    element the element to search
    '''
    
    for i in list:
        if list[i] == element:
            return True
            
    return False
    
def recSearch(aList, element):
    ''' 
    The exact same as search but with recursion
    '''

    if element == aList[0]:
        return True
    if len(aList) == 1:
        return False
   
    return recSearch(aList[1:], element)


def recBiSearch(aList, element):
    '''
    Binary seach for a sorted list
    aList a sorted list
    element the element to search
    '''

    if len(aList) == 1:
        return aList[0] == element
    
    mid = len(aList)/2
    
    if aList[mid] == element:
        return True
    
    if aList[mid] > element:
        return recBiSearch(aList[:mid], element)
    
    return recBiSearch(aList[mid:], element)
    
def test():
    myList = [1,2,3,7,8,9,11,15,16,18,22,23,25,29,33]
    myCharList = ['c','f','f','g','i','j','k','y','z']
    myStringList = ["fabio","gato","muito","vai","xarles","zip"]
    myStringListA = ["a","abs","acorn","add","adore","afair"]
    
    notMyList = [4,5,6,10,12,13,14,17,19,20,21,24,30]
    notCharList = ['a','b','d','h','l','m']
    notStringList = ["fato","gabo","muitos","vapor","zippo"]
    notStringListA = ["air","abo","arn","adiction","adoreble"]
    
    #falses
    for i in notMyList:
        print recBiSearch(myList, i)
    
    for c in notCharList:
        print recBiSearch(myCharList, c)

    for s in notStringList:
        print recBiSearch(myStringList, s)

    for s in notStringListA:
        print recBiSearch(myStringListA, s)
        
    #trues
    for i in myList:
        print recBiSearch(myList, i)
    
    for c in myCharList:
        print recBiSearch(myCharList, c)

    for s in myStringList:
        print recBiSearch(myStringList, s)

    for s in myStringListA:
        print recBiSearch(myStringListA, s)

test()