def isPalindrome(aString):
    '''
    aString: a string
    '''
    if len(aString) <= 1:
        return True
    
    if len(aString) == 2 and aString[0] == aString[1]:
        return True

    aString = aString.lower()
    letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    retString = ''

    for char in aString:
        if char in letters:
            retString += char

    aString = retString
 
    if aString[0] == aString[-1]:
        return isPalindrome(aString[1: -1])
    
    return False
print isPalindrome('a')
print isPalindrome('sabas')
print isPalindrome('sabias')
print isPalindrome('aaa')
print isPalindrome('socorram-me subi no onibus em marrocos')