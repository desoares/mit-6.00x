def dotProduct(listA, listB):
    '''
    listA: a list of numbers
    listB: a list of numbers of the same length as listA
    '''
    if len(listA) != len(listB):
        print 'Error, lists have worng length!'
        return None
       
    total = 0 
    
    for n in range(len(listA)):
        total += listA[n] * listB[n]

    return total
    
listA = [1, 2, 3]
listB = [4, 5, 6]

print dotProduct(listA, listB)