def f(a, b):
    return a + b

def dict_interdiff(d1, d2):
    '''
    d1, d2: dicts whose keys and values are integers
    Returns a tuple of dictionaries according to the instructions of the test
    '''
    
    inter = {}
    diff = {}
    d1_v = d1.keys()
    d2_v = d2.keys()

    for num in range(len(d1_v)):
        if d1_v[num] in d2_v:
            inter[d1_v[num]] = f(d1[d1_v[num]], d2[d2_v[num]])
        
        else:
            diff[d1_v[num]] = d1[d1_v[num]]

    for num in range(len(d2_v)):
        if d2_v[num] not in d1_v:
            diff[d2_v[num]] = d2[d2_v[num]]

    return (inter, diff, )

d1 = {1:30, 2:20, 3:30, 5:80}
d2 = {1:40, 2:50, 3:60, 4:70, 6:90}
#d1 = {1:30, 2:20, 3:30}
#d2 = {1:40, 2:50, 3:60}
    
print dict_interdiff(d1, d2)