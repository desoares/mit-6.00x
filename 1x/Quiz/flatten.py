def flatten(aList):
    '''
    aList: a list 
    Returns a copy of aList, which is a flattened version of aList 
    '''
    returnList = []

    def doIt(someList):
        if type(someList) != list:
            returnList.append(someList)

        else: 
            for item in someList:
                doIt(item)
    
    for item in aList:
        doIt(item)
            
    return returnList
   
print flatten([[1,'a',['cat'],2],[[[3]],'dog'],4,5]) 