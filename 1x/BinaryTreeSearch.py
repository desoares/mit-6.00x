class BinaryTreeSearch(object):
    def callBack(node, val):
        return node.value == val
    
    def ltCallBack(node, val):
        return val < node.value
    
    def depthFirstSearch(root, cb, val) :
        stack = [root]
        
        while len(stack):
            if cb(stack[0], val):
                return True
    
            temp = stack.pop(0)
    
            if temp.getRightBranch():
                stack.insert(0, temp.getRightBranch())
            if temp.getLeftBranch():
                stack.insert(0, temp.getLeftBranch())
    
        return False
    
    def breadthFirstSearch(root, cb, val):
        queue = [root]
    
        while len(queue):
            if cb(queue[0], val):
                return True
    
            temp = queue.pop(0)
    
            if temp.getLeftBranch():
                queue.append(temp.getLeftBranch())
            if temp.getRightBranch():
                queue.append(temp.getRightBranch())
    
        return False
    
    def tracePath(self, node):
        if not node.getParent():
            return [node]
    
        return [node] + self.tracePath(node.getParent())
        
        
    def depthFirstSearchPath(self, root , cb, val):
        stack = [root]
        
        while len(stack):
            if cb(stack[0], val):
                return self.tracePath(stack[0])
            
            temp = stack.pop(0)
            
            if temp.getRightBranch():
                stack.insert(0, temp.getRightBranch())
            if temp.getLeftBranch():
                stack.insert(0, temp.getLeftBranch())        
    
        return False
        
    def depthFirstOrdered(root, cb, ltCb, val):
        stack = [root]
        
        while len(stack):
            if cb(stack[0], val):
                return True
    
            if ltCb(stack[0], val):
    
                temp = stack.pop(0)
                if temp.getLeftBranch():
                    stack.insert(0, temp.getLeftBranch())
                    #print stack[0].value
            else:
                temp = stack.pop(0)
                if temp.getRightBranch():
                    stack.insert(0, temp.getRightBranch())
                    #print stack[0].value
    
        return  False