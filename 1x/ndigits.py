def ndigits(x):
    '''
    x is an integer lesser, equal or greater than 0
    return the number of digits in x
    '''
    x = abs(x)

    if x < 10:
        return 1

    return 1 + ndigits(x/10)

