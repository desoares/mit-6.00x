def getGuessedWord(secretWord, lettersGuessed):
    '''
    secretWord: string, the word the user is guessing
    lettersGuessed: list, what letters have been guessed so far
    returns: string, comprised of letters and underscores that represents
      what letters in secretWord have been guessed so far.
    '''
    returnStr = ''
    for letter in secretWord:
        if letter in lettersGuessed:
            returnStr += letter
        else:
            returnStr += '_'
    return returnStr
    
word = 'cooperacao'
guess = ['a', 'e', 'i', 'c', 'f']

test = getGuessedWord(word, guess)

print test