class BinaryTree(object):
    def __init__(self, value):
        self.value = value
        self.leftBranch = None
        self.rightBranch = None
        self.parent = None
        
    def setLeftBranch(self, node):
        self.leftBranch = node
        node.setParent(self)
    
    def setRightBranch(self, node):
        self.rightBranch = node
        node.setParent(self)
        
    def setParent(self, parent):
        self.parent = parent

    def setChildren(self, leftNode, rightNode):
        self.setLeftBranch(leftNode)
        self.setRightBranch(rightNode)
    
    def getValue(self):
        return self.value
    
    def getLeftBranch(self):
        return self.leftBranch
    
    def getRightBranch(self):
        return self.rightBranch
    
    def getParent(self):
        return self.parent
    
    def __str__(self):
        return '< ' + str(self.value) + ' L: ' + str(self.leftBranch) + ' R: ' + str(self.rightBranch) + ' >'