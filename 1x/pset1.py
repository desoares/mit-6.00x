s = 'code is assuming all vowels will be on lower case as said in the pset text'
def coutVowels(s): 
    '''
    code is assuming all vowels will be on lower case as said in the pset text
    '''
    vowels = 'aeiou' 
    totalVowels = 0
    #if not(s.islower()):
    #    s = s.lower()

    for vowel in vowels:
        totalVowels += s.count(vowel)

    return totalVowels

print "Number of vowels: " + str(coutVowels(s))