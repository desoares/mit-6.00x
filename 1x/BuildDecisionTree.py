import BinaryTree

def buildDecisionTree(sofar, todo):
        if len(todo) == 0:
            return BinaryTree(sofar)
        
        else:
            witheld = buildDecisionTree(sofar + todo[0], todo[1:])
            withoutelt = buildDecisionTree(sofar. todo[:1])
            here = BinaryTree.BinaryTree(sofar)
            here.setChildren(witheld, withoutelt)
            
            return here
            
def depthFirstSearchDecisionTree(root, cb, constrainCb):
    stack = [root]
    best = None
    visited = 0
    
    while len(stack):
        visited += 1
        
        if constrainCb(stack[0].getValue()):
            if not best:
                best = stack[0]
            
            elif cb(stack[0].getvalue()) > cb(best.getValue()):
                best = stack[0]
            temp = stack.pop(0)
            
            if temp.getRightBranch():
                stack.insert(0, temp.getRightBranch())
                
            if temp.getLeftBranch():
                stack.insert(0, temp.getLeftBranch())
                
    else:
        stack.pop(0)
        
    print 'Visited: ' + str(visited)
    return best
                
def breadFristSearchBTGoodEnought(root, cb, constrainCb, stop):
    stack = [root]
    best = None
    visited = 0

    while len(stack):
        visited += 1

        if constrainCb(stack[0].getValue()):
            if not best:
                best = stack[0]
                
            elif cb(stack[0].getValue()) > cb(best.getValue()):
                best = stack[0]

            if stop(best.getValue()):
                return best
            
            temp = stack.pop(0)
            
            if temp.getRightBranch():
                stack.insert(0, temp.getRightBranch())
                
            if temp.getLeftBranch():
                stack.insert(0, temp.getLeftBranch())
            
            else:
                stack.pop(0)
            
            print 'Visited: ' + str(visited)
            return best

#implicit Search
def decisionTreeImplicit(consider, available):

    if consider == [] or available == 0:
        result = (0, ())

    elif consider[0][1] > available:
        result = decisionTreeImplicit(consider[1:], available)

    else:
        nextItem = consider[0]
        withVal, withToTake = decisionTreeImplicit(consider[1:], available - nextItem[1])
        withVal += nextItem[0]
        withoutVal, withoutToTake = decisionTreeImplicit(consider[1:], available)

        if withVal > withoutVal:
            result  = (withVal, withoutToTake) 
            
    return result
        