class intSet(object):
    """An intSet is a set of integers
    The value is represented by a list of ints self.vals
    Each int in the set occurs in self.vals exactly once"""
    
    def __init__(self):
        """Create an empty set o integers"""
        self.vals = []
        
    def insert(self, e):
        """Assumes e is an integer and inserts e into self"""
        if not e in self.vals:
            self.vals.append(e)
        
    def __str__(self):
        """Returns a string representation of self"""
        self.vals.sort()
        return '{' + ','.join([str(e) for e in self.vals]) + '}'
    
    def member(self, e):
        return e in self.vals
    
    def remove(self, e):
        """Assumes e is an integer and removes e from self
        Raises ValueError if e is not in self"""
        
        try:
            self.vals.remove(e)
        except:
            raise ValueError(str(e) + ' not found')
    
    def intersect(self, other):
        result = intSet()
        
        for i in self.vals:
            if i in other.vals:
                result.insert(i)
        
        return result
    
    def __len__(self):
        return len(self.vals)
        
l1 = intSet()
l2 = intSet()

l1.insert(1)
l1.insert(2)
l1.insert(3)

l2.insert(2)
l2.insert(3)
l2.insert(4)

print l1.intersect(l2)
print l2.__len__()