balance = 4213
annualInterestRate = 0.2
monthlyPaymentRate = 0.04

monthInterestRate  = annualInterestRate / 12
monthInterest = 0.0
total = 0

for num in range(12):

    monthlyPayment = balance * monthlyPaymentRate
    balance -= monthlyPayment
    total += monthlyPayment
    monthInterest = (balance * monthInterestRate)
    balance += monthInterest

    print "Month: " + str(num + 1)
    print "Minimum monthly payment: " + str(round(monthlyPayment, 2))
    print "Remaining balance: " + str(round(balance, 2))  

print "Total paid: " + str(round(total, 2))
print "Remaining balance: " + str(round(balance, 2))