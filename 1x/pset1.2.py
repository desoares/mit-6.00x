s = 'obeboobbobbobkooboobbobobobobobobbbobboob'
def countBob(s): 
    '''
    code is assuming all vowels will be on lower case as said in the pset text
    '''
    totalBobs = 0
    index = 0
    
    for letter in s:
        index += 1
        if letter == 'b':
            if len(s) > index+1 :
                if s[index] == 'o' and s[index+1] == 'b':
                    totalBobs += 1   

    return totalBobs

print "Number of times bob occurs is: " + str(countBob(s))