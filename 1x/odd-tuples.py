def oddTuples(aTup):
    '''
    aTup: a tuple
    
    returns: tuple, every other element of aTup.
    '''
    i = 0
    newTup = ()
    for item in aTup:
        if i % 2 == 0:
            newTup += (item,)

        i += 1
            
    return newTup
    
test = oddTuples(('I', 'am', 'a', 'test', 'tuple'))

print test