class Queue(object):
    
    def __init__(self):
        self.queue = []

    def insert(self, e):
        self.queue.append(e)
    
    def remove(self):
        try:
            deleted = self.queue.pop(0)
            return deleted

        except:
            raise ValueError()

q = Queue()
q.insert(5)
q.insert(6)
print q.remove()
q.insert(7)
print q.remove()
print q.remove()
#q.remove()
