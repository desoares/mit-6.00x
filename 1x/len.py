def lenIter(aStr):
    count = 0
    for c in aStr:
        count += 1

    return count
    
test = lenIter('a')
print test

test = lenIter('ab')
print test

test = lenIter('abc')
print test

test = lenIter('abcd')
print test
