def nfruits(dic, patt):
    assert len(dic) > 0 and len (patt) > 0, 'Bad argument error'

    if len(patt) == 1:
        dic[patt] -= 1
        return greater(dic)
      
    for letter in patt:   
        dic[letter] += 1
        
    dic[patt[0]] -= 2
    return nfruits(dic, patt[1:])

def greater(dic):
    keys = dic.keys()
    hi = 0
    
    for key in keys:
        if dic[key] > hi:
            hi = dic[key]

    return hi
    
    
dic = {'A': 1, 'B': 2, 'C': 3}
patt = 'AC'

print nfruits({'Y': 6, 'G': 8}, 'YGYY')