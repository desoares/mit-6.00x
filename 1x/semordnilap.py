def semordnilap(str1, str2):
    '''
    str1: a string
    str2: a string
    
    returns: True if str1 and str2 are semordnilap;
             False otherwise.
    '''    
    str1 = str1.lower()
    str2 = str2.lower()
    length = len(str1)
    
    if len(str1) <= 1 or len(str2) <= 1:
        return False
    if len(str1) != len(str2):
        return False
    if str1 == str2:
        return False
    if len(str1) == 2 and str1[0] == str2[1] and str1[1] == str2[0]:
        return True
    
    if str1[0] == str2[length - 1]:
        return semordnilap(str1[1:length], str2[:-1])
    
    return False

test = semordnilap('gateman', 'nametag')
print test

