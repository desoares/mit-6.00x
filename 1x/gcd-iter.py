def gcdRecur(a, b):
    '''
    a, b: positive integers
    
    returns: a positive integer, the greatest common divisor of a & b.
    '''
    hi = a
    lo = b

    if b > a:
        hi = b
        lo = a

    if lo == 0:
        return a
    
    if hi % lo == 0:
        return lo
        
    return gcdRecur(lo, hi % lo)
        

test = gcdRecur(150, 18)

print test