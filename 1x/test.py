
"""
class Spell(object):
    def __init__(self, incantation, name):
        self.name = name
        self.incantation = incantation

    def __str__(self):
        return self.name + ' ' + self.incantation + '\n' + self.getDescription()
              
    def getDescription(self):
        return 'No description'
    
    def execute(self):
        print self.incantation    


class Accio(Spell):
    def __init__(self):
        Spell.__init__(self, 'Accio', 'Summoning Charm')

class Confundo(Spell):
    def __init__(self):
        Spell.__init__(self, 'Confundo', 'Confundus Charm')

    def getDescription(self):
        return 'Causes the victim to become confused and befuddled.'

def studySpell(spell):
    print spell

spell = Accio()
spell.execute()
studySpell(spell)
studySpell(Confundo())

class A(object):
    def __init__(self):
        self.a = 1
    def x(self):
        print "A.x"
    def y(self):
        print "A.y"
    def z(self):
        print "A.z"

class B(A):
    def __init__(self):
        A.__init__(self)
        self.a = 2
        self.b = 3
    def y(self):
        print "B.y"
    def z(self):
        print "B.z"

class C(object):
    def __init__(self):
        self.a = 4
        self.c = 5
    def y(self):
        print "C.y"
    def z(self):
        print "C.z"

class D(C, B):
    def __init__(self):
        C.__init__(self)
        B.__init__(self)
        self.d = 6
    def z(self):
        print "D.z"
        
obj = D()
print obj.a
"""
class AdoptionCenter(object):
    """
    The AdoptionCenter class stores the important information that a
    client would need to know about, such as the different numbers of
    species stored, the location, and the name. It also has a method to adopt a pet.

    def __init__(self, name, species_types, location):
        self.name = str(name)
        self.species_types = dict(species_types)
        self.location = (float(location[0]), float(location[1]))

    def get_number_of_species(self, animal):
        return self.species_types[animal]

    def get_location(self):
        return self.location

    def get_species_count(self):
        ret_dict = {}

        for specie in self.species_types:
            if self.species_types[specie] != 0:
                ret_dict[specie] = self.species_types[specie]
        return ret_dict    
    
    def get_name(self):
        return self.name

    def adopt_pet(self, species):
        self.species_types[species] -= 1

        #if self.species_types[species] < 1:
            #del self.species_types[species]

test = AdoptionCenter("Test", {"Dog": 1, "Cat": 3}, (1,1))
test.adopt_pet("Dog")
name = test.get_species_count()
print name["Cat"]
"""
length = 9
center = 3
numb = 0

ws = length - (center * 2 + 1)
print ws