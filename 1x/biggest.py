def biggest(aDict):
    '''
    aDict: A dictionary, where all the values are lists.

    returns: The key with the largest number of values associated with it
    '''   
    if len(aDict) == 0:
        return None

    hi = aDict.keys()[0]
    total = len(hi)

    for key in aDict:
        total = len(aDict[key]) 

        if total > len(aDict[hi]):
            hi = key

    if total == 0:
        return None

    return hi
    
animals = { 'a': []}

print biggest(animals)