import random as rand
import string

class AdoptionCenter(object):
    """
    The AdoptionCenter class stores the important information that a
    client would need to know about, such as the different numbers of
    species stored, the location, and the name. It also has a method to adopt a pet.
    """
    def __init__(self, name, species_types, location):
        self.name = str(name)
        self.species_types = dict(species_types)
        self.location = (float(location[0]), float(location[1]))

    def get_number_of_species(self, animal):
        return self.species_types[animal]

    def get_location(self):
        return self.location

    def get_species_count(self):
        ret_dict = {}

        for specie in self.species_types:
            if self.species_types[specie] != 0:
                ret_dict[specie] = self.species_types[specie]

        return ret_dict    
    
    def get_name(self):
        return self.name

    def adopt_pet(self, species):
        self.species_types[species] -= 1
        


class Adopter(object):
    """ 
    Adopters represent people interested in adopting a species.
    They have a desired species type that they want, and their score is
    simply the number of species that the shelter has of that species.
    """
    def __init__(self, name, desired_species):
        self.name = name
        self.desired_species = desired_species

    def get_name(self):
        return self.name

    def get_desired_species(self):
        return self.desired_species

    def get_score(self, adoption_center):
         center_species = adoption_center.get_species_count()
         center_species_names = center_species.keys()
         num_desired = 0

         for specie in center_species_names:
             if specie == self.get_desired_species():
                 num_desired = center_species[specie]

         return float(1 * num_desired)


class FlexibleAdopter(Adopter):
    """
    A FlexibleAdopter still has one type of species that they desire,
    but they are also alright with considering other types of species.
    considered_species is a list containing the other species the adopter will consider
    Their score should be 1x their desired species + .3x all of their desired species
    """
    def __init__(self, name, desired_species, considered_species):
        Adopter.__init__(self, name, desired_species)
        self.considered_species = considered_species #list of strings

    def get_score(self, adoption_center):
        adopter_score = Adopter.get_score(self, adoption_center)
        center_species = adoption_center.get_species_count()
        center_species_names = center_species.keys()
        total_considered = 0

        for species in self.considered_species:
            if species in center_species_names:
                total_considered += center_species[species]

        return adopter_score + 0.3 * total_considered
        

class FearfulAdopter(Adopter):
    """
    A FearfulAdopter is afraid of a particular species of animal.
    If the adoption center has one or more of those animals in it, they will
    be a bit more reluctant to go there due to the presence of the feared species.
    Their score should be 1x number of desired species - .3x the number of feared species
    """
    def __init__(self, name, desired_species, feared_species):
        Adopter.__init__(self, name, desired_species)
        self.feared_species = feared_species
    
    def get_score(self, adoption_center):
        adopter_score = Adopter.get_score(self, adoption_center)
        center_species = adoption_center.get_species_count()
        center_species_names = center_species.keys()
        total_feared = 0

        for species in center_species_names:
            if species == self.feared_species:
                total_feared = center_species[species]

        total = adopter_score - 0.3 * total_feared

        if total < 0.0:
            return 0.0

        return total
        


class AllergicAdopter(Adopter):
    """
    An AllergicAdopter is extremely allergic to a one or more species and cannot
    even be around it a little bit! If the adoption center contains one or more of
    these animals, they will not go there.
    Score should be 0 if the center contains any of the animals, or 1x number of desired animals if not
    """
    def __init__(self, name, desired_species, allergic_species):
        Adopter.__init__(self, name, desired_species)
        self.allergic_species = allergic_species#list of strings
    
    def get_score(self, adoption_center):
        adopter_score = Adopter.get_score(self, adoption_center)
        center_species = adoption_center.get_species_count()
        center_species_names = center_species.keys()


        for species in center_species_names:
            if species in self.allergic_species:
                return 0.0

        return adopter_score


class MedicatedAllergicAdopter(AllergicAdopter):
    """
    A MedicatedAllergicAdopter is extremely allergic to a particular species
    However! They have a medicine of varying effectiveness, which will be given in a dictionary
    To calculate the score for a specific adoption center, we want to find what is the most allergy-inducing species that the adoption center has for the particular MedicatedAllergicAdopter. 
    To do this, first examine what species the AdoptionCenter has that the MedicatedAllergicAdopter is allergic to, then compare them to the medicine_effectiveness dictionary. 
    Take the lowest medicine_effectiveness found for these species, and multiply that value by the Adopter's calculate score method.
    """
    def __init__(self, name, desired_species, alergic_species, medicine_effectiveness):
        AllergicAdopter.__init__(self, name, desired_species, alergic_species)
        self.medicine_effectiveness = medicine_effectiveness
        
    def get_score(self, adoption_center):
        adopter_score = Adopter.get_score(self, adoption_center)
        center_species = adoption_center.get_species_count()
        center_species_names = center_species.keys()
        meds = self.medicine_effectiveness
        medications = meds.keys()

        lo = 1.0
        
        for species in medications:
            if species in center_species_names:
                if meds[species] < lo:
                    lo = meds[species]

        return adopter_score * lo

class SluggishAdopter(Adopter):
    """
    A SluggishAdopter really dislikes travelleng. The further away the
    AdoptionCenter is linearly, the less likely they will want to visit it.
    Since we are not sure the specific mood the SluggishAdopter will be in on a
    given day, we will asign their score with a random modifier depending on
    distance as a guess.
    Score should be
    If distance < 1 return 1 x number of desired species
    elif distance < 3 return random between (.7, .9) times number of desired species
    elif distance < 5. return random between (.5, .7 times number of desired species
    else return random between (.1, .5) times number of desired species
    """
    def __init__(self, name, desired_species, location):
        Adopter.__init__(self, name, desired_species)
        self.location = location
    
    
    def get_score(self, adoption_center):
        import math
        import random
        i = self.location
        ac = adoption_center.get_location()
        
        d = math.sqrt((i[0] - ac[0])**2 + (i[1] - ac[1])**2)
        adopter_score = Adopter.get_score(self, adoption_center)
        
        if d < 1:
            return adopter_score
        
        if 1 <= d < 3:
            return adopter_score * random.uniform(0.7, 0.9)
        
        if 3 <= d < 5:
            return adopter_score * random.uniform(0.5, 0.7)

        return adopter_score * random.uniform(0.1, 0.5)
        
    
def get_ordered_adoption_center_list(adopter, list_of_adoption_centers):
    """
    The method returns a list of an organized adoption_center such that the scores for each AdoptionCenter to the Adopter will be ordered from highest score to lowest score.
    """
    list_of_adoption_centers.sort(key = lambda adoption_center: adopter.get_score(adoption_center), reverse = True)
    return list_of_adoption_centers
    
def get_adopters_for_advertisement(adoption_center, list_of_adopters, n):
    """
    The function returns a list of the top n scoring Adopters from list_of_adopters (in numerical order of score)
    """
    list_of_adopters.sort(key = lambda adopter: adopter.name)
    list_of_adopters.sort(key = lambda adopter: adopter.get_score(adoption_center), reverse = True)
    return list_of_adopters[:n]


adopter = MedicatedAllergicAdopter("One", "Cat", ['Dog', 'Horse'], {"Dog": .5, "Horse": 0.2})
adopter2 = Adopter("Two", "Cat")
adopter3 = FlexibleAdopter("Three", "Horse", ["Lizard", "Cat"])
adopter4 = FearfulAdopter("Four","Cat","Dog")
adopter5 = SluggishAdopter("Five","Cat", (1,2))
adopter6 = AllergicAdopter("Six", "Cat", "Dog") 

ac = AdoptionCenter("Place1", {"Mouse": 12, "Dog": 2}, (1,1))
ac2 = AdoptionCenter("Place2", {"Cat": 12, "Lizard": 2}, (3,5))
ac3 = AdoptionCenter("Place3", {"Horse": 25, "Dog": 9}, (-2,10))

# how to test get_adopters_for_advertisement

# you can print the name and score of each item in the list returned
for adopter in get_adopters_for_advertisement(ac, [adopter, adopter2, adopter3, adopter4, adopter5, adopter6], 10):
    print adopter.name,
    print adopter.get_score(ac)


adopter4 = FearfulAdopter("Four","Cat","Dog")
adopter5 = SluggishAdopter("Five","Cat", (1,2))
adopter6 = AllergicAdopter("Six", "Lizard", "Cat") 

ac = AdoptionCenter("Place1", {"Cat": 12, "Dog": 2}, (1,1))
ac2 = AdoptionCenter("Place2", {"Cat": 12, "Lizard": 2}, (3,5))
ac3 = AdoptionCenter("Place3", {"Cat": 40, "Dog": 4}, (-2,10))
ac4 = AdoptionCenter("Place4", {"Cat": 33, "Horse": 5}, (-3,0))
ac5 = AdoptionCenter("Place5", {"Cat": 45, "Lizard": 2}, (8,-2))
ac6 = AdoptionCenter("Place6", {"Cat": 23, "Dog": 7, "Horse": 5}, (-10,10))

# how to test get_ordered_adoption_center_list
for center in get_ordered_adoption_center_list(adopter4, [ac,ac2,ac3,ac4,ac5,ac6])                            :
    print center.name,
    print adopter4.get_score(center)
# you can print the name and score of each item in the list returned