# -*- coding: utf-8 -*-
find = 1
totalPrimes = 0

while totalPrimes < 1000:
    prime = 0
    for num in range(find):
        if num > 0:
            if find % num == 0:
                prime += 1
            
    if prime == 1:
        totalPrimes += 1
        lastPrime = find
        if totalPrimes % 500 == 0:
            print str(lastPrime) + " is prime. Total: " + str(totalPrimes)

    find += 1

print lastPrime