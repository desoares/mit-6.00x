def isIn(char, aStr):
    if char == '':
        'Print that\'s not going to work whith no character to match'
        return False

    if char == aStr:
        return True
    
    if aStr == '' or (len(aStr) == 1 and char != aStr):
        return False

    guess = len(aStr) / 2
    
    if char == aStr[guess]:
        return True        

    if char > aStr[guess]:
        lo = len(aStr) / 2
        hi = len(aStr)
    else:
        lo = 0
        hi = len(aStr) / 2

    return isIn(char, aStr[lo:hi])
    
test = isIn('y', 'abcdefghx')
print test
