# -*- coding: utf-8 -*-
prevM = 0
currM = 1
nextM = 0
fibSum = 1 

for num in range(48):
    nextM = currM + prevM
    prevM = currM
    currM = nextM
    fibSum += nextM
    #print fibSum

print fibSum