def genPrimes():
    yield 2
    yield 3
    
    test = 5
    
    while True:
        prime = True

        for i in range(3, test):
            if test % i == 0:
                prime = False                
                break
        
        if prime:
            yield test
            
        test += 2

test = genPrimes()
print test.next()
print test.next()
print test.next()
print test.next()
print test.next()
print test.next()
print test.next()
print test.next()
print test.next()
print test.next()
print test.next()
print test.next()
print test.next()
print test.next()
print test.next()