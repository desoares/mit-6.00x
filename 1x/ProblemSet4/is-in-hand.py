myList = ['world', 'yes', 'fabio']

#
# Problem 3-2: teste wors valid in the hand
#
def isInHand(hand, word):
    
    for letter in word:

        try: 

            if word.count(letter) > hand[letter]:
                return False
        except KeyError:
            return False
            
    return True
    

def isValidWord(word, hand, wordList):
    """
    Returns True if word is in the wordList and is entirely
    composed of letters in the hand. Otherwise, returns False.

    Does not mutate hand or wordList.
   
    word: string
    hand: dictionary (string -> int)
    wordList: list of lowercase strings
    """
    
    if not isInHand(hand, word):
        return False
    
    if not word in wordList:
        return False
    
    return True
    
hand = {'f': 4, 'a': 1, 'e': 1, 'y': 4, 'h': 1, 'z': 0, 'x': 1, 's': 1}
word = 'fabio'

print isValidWord(word, hand, myList)