def updateHand(hand, word):
    """
    Assumes that 'hand' has all the letters in word.
    In other words, this assumes that however many times
    a letter appears in 'word', 'hand' has at least as
    many of that letter in it. 

    Updates the hand: uses up the letters in the given word
    and returns the new hand, without those letters in it.

    Has no side effects: does not modify hand.

    word: string
    hand: dictionary (string -> int)    
    returns: dictionary (string -> int)
    """
    resultHand = {}

    for item in hand:
        resultHand[item] = hand[item]
    
    for letter in word:
        if resultHand[letter]:
            resultHand[letter] -= 1

    return resultHand
    
    
hand = {'a':3 ,'c':1 ,'h':2 ,'k':3, 'g':4, 'e': 2}
word = 'achkgeahkggekagg'

print hand
print updateHand(hand, word)
