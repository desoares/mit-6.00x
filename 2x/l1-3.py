import pylab
FILE = '/home/desoares/projetos/mitx-6.00/2x/julyTemps.txt'
try: 
    inFile = open(FILE, 'r', 0)
except:
    'Could not find File'
    
diffTemps = []

for line in inFile:
    line = line.split()
    if len(line) == 3 and line[0].isdigit():
        diffTemps.append(int(line[2]) - int(line[0]))


pylab.figure(1)
pylab.plot(range(1, 32), diffTemps)
pylab.title('Day by Day Ranges in Temperature in Boston in July 2012')
pylab.xlabel('Days')
pylab.ylabel('Temperature Ranges')
#pylab.show()

test = "a, b, c, d, e"

print test.split(',')