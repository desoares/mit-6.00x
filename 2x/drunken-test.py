import Location,Field,Drunk

def walk(f, d, steps):
    start = f.getLoc(d)
    
    for s in range(steps):
        f.moveDrunk(d)
        
    return start.distFrom(f.getLoc(d))
    
def simWalks(steps, trials):
    homer = Drunk.UsualDrunk('Homer')
    origin = Location.Location(0, 0)
    distances = []
    
    for t in range(trials):
        f = Field.Field()
        f.addDrunk(homer, origin)
        distances.append(walk(f, homer, steps))
        
    return distances
    
def drunkTest(trial = 20):
    for steps in [10, 100, 1000, 10000]:
        distances = simWalks(steps, trial)
        
        print "Randon walk of " + str(steps) + ' steps'
        print "Mean = ", sum(distances) / len(distances)
        print "Max = ", max(distances), " Min = ", min(distances)
        
        
drunkTest()