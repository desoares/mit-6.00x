import random

def noReplacementSimulation(numTrials):
    '''
    Runs numTrials trials of a Monte Carlo simulation
    of drawing 3 balls out of a bucket containing
    3 red and 3 green balls. Balls are not replaced once
    drawn. Returns the a decimal - the fraction of times 3 
    balls of the same color were drawn.
    '''
    allTheSame = 0

    for i in range(numTrials):
        bucket = ['r','g','r','g','r','g']
        draw = []
        
        for i in range(3):
            length = len(bucket) - 1
            chance = random.randint(0, length)
            draw.append(bucket.pop(chance))
        
        if draw[0] == draw[1] == draw[2]:
            allTheSame += 1
        
    return float(allTheSame)/numTrials
    
print noReplacementSimulation(100000)

noReplacementSimulation(1000)
##Solution give by the course

def oneTrial():
    '''
    Simulates one trial of drawing 3 balls out of a bucket containing
    3 red and 3 green balls. Balls are not replaced once
    drawn. Returns True if all three balls are the same color,
    False otherwise.
    '''
    balls = ['r', 'g', 'r', 'g', 'r', 'g']
    drawnBalls = []

    for i in range(3):
        # For three trials, pick a ball
        ball = random.choice(balls)
        # Remove the chosen ball from the set of balls
        balls.remove(ball)
        # and add it to a list of balls we picked
        drawnBalls.append(ball)
    # If the first ball is the same as the second AND the second is the same as the third,
    #  we know all three must be the same color.
    if drawnBalls[0] == drawnBalls[1] == drawnBalls[2]:
        return True

    return False

def noReplacementSimulation(numTrials):
    '''
    Runs numTrials trials of a Monte Carlo simulation
    of drawing 3 balls out of a bucket containing
    3 red and 3 green balls. Balls are not replaced once
    drawn. Returns the a decimal - the fraction of times 3 
    balls of the same color were drawn.
    '''
    numTrue = 0
    for trial in range(numTrials):
        if oneTrial():
            numTrue += 1

    return float(numTrue)/float(numTrials)
