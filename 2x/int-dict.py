class intDict(object):
    """A dictionary with integer keys"""
    
    def __init__(self, nBuckets):
        """Create an empty dictionary"""
        
        self.buckets = []
        self.nBuckets = nBuckets
        
        for i in range(nBuckets):
            self.buckets.append([])
    
    def addEntry(self, key, val):
        """Assumes key is an int. Adds a new entry"""
        
        hashBucket = self.buckets[key % self.nBuckets]
        length = len(hashBucket)
        
        for i in range(length):
            if hashBucket[i][0] ==  key:
                hashBucket[i] = (key, val)
                return
        
        hashBucket.append((key, val))
        
    def getValue(self, key):
        """Assumes key is an int.
        Returns entry associated to key"""
        
        hashBucket = self.buckets[key % self.nBuckets]
        
        for e in hashBucket:
            if e[0] == key:
                return e[1]
                
    def __str__(self):
        res =  '{ '
        
        for b in self.buckets:
            for t in b:
                res += str(t[0]) + ':' + str(t[1]) + ', '
                
        return res[:-1] + ' }' #res[:-1] removes the last coma