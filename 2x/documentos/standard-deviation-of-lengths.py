def stdDevOfLengths(L):
    """
    L: a list of strings

    returns: float, the standard deviation of the lengths of the strings,
      or NaN if L is empty.
    """
    length = float(len(L))
    if not length:
        return float('NaN')

    val = 0
    devSqr = 0
    
    for s in L:
        val += len(s)
        mean = val / length

    for s in L:
        devSqr += (len(s) - mean)**2
    
    varia = devSqr / length

    return varia**0.5

L = ['a', 'z', 'p']
L = ['apples', 'oranges', 'kiwis', 'pineapples']
print stdDevOfLengths(L)