def stdDev(X):
    
    mean = sum(X)/float(len(X))
    tot = 0.0
    
    for x in X:
        tot += (x - mean)**2
    
    return (tot/len(X))**0.5