#import random

def strToInt(s):
    number = ''
    
    for c in s:
        number = number + str(ord(c))
        
    return int(number)

print 'Index: ', strToInt('a')
print 'Index: ', strToInt('Fabio')

def hashStr(s, tableSize = 101):
    number = ''
    
    for c in s:
        number = number + str(ord(c))
        
    return int(number) % tableSize

print 'Index: ', hashStr('a', 100)
print 'Index: ', hashStr('Fabio', 100)