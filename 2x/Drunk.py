class Drunk(object):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return 'This drunk is named ' + self.name
        
import random

class UsualDrunk(Drunk):
    
    def __init__(self, name):
        Drunk(self, name)
    def takeStep(self):
        stepChoices = [(0.0, 1.0), (0.0, -1.0), (1.0, 0.0), (-1.0, 0.0)]
        
        return random.choice(stepChoices)