import random

def rollDie():
    return random.choice(range(1, 7))

def rollN(n):
    result = ''
    
    for i in range(n):
        result = result + str(rollDie())
    
    return result
    
def getTarget(goal):
    tries = 0
    rolls = len(goal)
    
    while True:
        tries += 1
        result = rollN(rolls)
        
        if result == goal:
            return tries

def runSim(goal, tries):
    total = 0
    
    for i in range(tries):
        total += getTarget(goal)
    
    averageTries = total / float(tries)
    print 'Probability = ', 1/averageTries

runSim('11111', 1000)