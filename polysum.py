#Impot math lib
import math

def polysum(n, s):
    '''
    n sides of the plygon int
    s size of each side float
    '''
    #Calculate the square of the prerimeter
    perSqr = (n * s)**2
    #Calculate the area of a polygon
    area = (0.25 * n * (s**2))/math.tan(math.pi/n)
    #Return the sum o both
    return round(perSqr + area, 4)